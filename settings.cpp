#include "settings.h"

#include <QSettings>

Settings::Settings(Base &base, Content &content, QObject *parent) :
    QObject(parent),
    m_base(base),
    m_content(content),
    m_lastUsedName("")
{

}

void Settings::saveLast(const QString &description)
{
    QString temp = m_lastUsedName;
    QSettings settings("E-Liquid Calculator");
    settings.setValue("lastUsedName", m_lastUsedName);

    save("lastUsed", description);
    m_lastUsedName = temp;
}

void Settings::loadLast()
{
    load("lastUsed");

    QSettings settings("E-Liquid Calculator");
    m_lastUsedName = settings.value("lastUsedName", "").toString();
}

void Settings::load(const QString &name)
{
    QSettings settings("E-Liquid Calculator");
    settings.beginGroup(name);

    m_base.setNicotine( settings.value("baseNicotine", 100.0).toDouble() );
    m_base.setPG( settings.value("basePG", 0.0).toDouble() );
    m_base.setVG( settings.value("baseVG", 100.0).toDouble() );
    m_base.setWater( settings.value("baseWater", 0.0).toDouble() );

    m_content.setNicotine( settings.value("contentNicotine", 6.0).toDouble() );
    m_content.setPG( settings.value("contentPG", 45.0).toDouble() );
    m_content.setVG( settings.value("contentVG", 45.0).toDouble() );
    m_content.setWater( settings.value("contentWater", 10.0).toDouble() );
    m_content.setVolume( settings.value("contentVolume", 10.0).toDouble() );
    m_content.setFlavor( settings.value("contentFlavor", 5.0).toDouble() );
    settings.endGroup();

    m_lastUsedName = name;
}

void Settings::save(const QString &name, const QString &description)
{
    QSettings settings("E-Liquid Calculator");
    settings.beginGroup(name);

    settings.setValue("description", description );

    settings.setValue("baseNicotine", m_base.nicotine() );
    settings.setValue("basePG", m_base.PG() );
    settings.setValue("baseVG", m_base.VG() );
    settings.setValue("baseWater", m_base.water() );

    settings.setValue("contentNicotine", m_content.nicotine() );
    settings.setValue("contentPG", m_content.PG() );
    settings.setValue("contentVG", m_content.VG() );
    settings.setValue("contentWater", m_content.water() );
    settings.setValue("contentVolume", m_content.volume() );
    settings.setValue("contentFlavor", m_content.flavor() );

    settings.endGroup();

    m_lastUsedName = name;
}

void Settings::remove(const QString &name)
{
    QSettings settings("E-Liquid Calculator");
    settings.remove(name);
}

bool Settings::contains(const QString &name)
{
    return getSavings().contains(name);
}

QStringList Settings::getSavings() const
{
    QSettings settings("E-Liquid Calculator");

    QStringList res = settings.childGroups();
    QStringList reserved = reservedWords();
    for (auto it=reserved.begin(); it != reserved.end(); ++it)
        res.removeAll( *it );

    return res;
}

QString Settings::getDescription(const QString name) const
{
    QSettings settings("E-Liquid Calculator");
    settings.beginGroup(name);
    return settings.value("description", "").toString();
}

QString Settings::getDescription() const
{
    return getDescription("lastUsed");
}

QStringList Settings::reservedWords() const
{
    QStringList res;
    res << "lastUsed"
        << "lastUsedName";

    return res;
}

QString Settings::lastUsedName() const
{
    return m_lastUsedName;
}

void Settings::clearLastUsedName()
{
    m_lastUsedName.clear();
}
