#ifndef BASE_H
#define BASE_H

#include "liquid.h"

class Base : public Liquid
{
    Q_OBJECT
public:
    explicit Base(QObject *parent = 0);

    double nicotine() const;

public slots:
    void setNicotine(double nicotine);

signals:
    void nicotineChanged(double value);

protected:
    double m_nicotine;
    double volumeValidation(double value);
};

#endif // BASE_H
