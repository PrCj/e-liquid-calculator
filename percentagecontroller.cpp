#include "percentagecontroller.h"

#include "inputwidget.h"

#define EPSILON 0.000001

PercentageController::PercentageController(QObject *parent) :
    QObject(parent),
    m_isEnabled(false),
    m_inProcess(false)
{

}

void PercentageController::addWidget(InputWidget *widget)
{
    m_widgetsList.append(widget);
    connect(widget, SIGNAL(valueChanged(double)), this, SLOT(widgetValueChanged(double)) );
}

void PercentageController::removeWidget(InputWidget *widget)
{
    m_widgetsList.removeAll(widget);
    disconnect(widget, SIGNAL(valueChanged(double)), this, SLOT(widgetValueChanged(double)) );
}

void PercentageController::widgetValueChanged(double value)
{
    Q_UNUSED(value);

    if (!m_isEnabled)
        return;

    if (m_inProcess)
        return;

    InputWidget *widget = qobject_cast<InputWidget *>( sender() );
    if (m_usedWidgetsHistory.isEmpty() || m_usedWidgetsHistory.last() != widget)
        m_usedWidgetsHistory.push_back(widget);
    if (m_usedWidgetsHistory.count() > 2)
        m_usedWidgetsHistory.pop_front();

    modifyWidgetsValues();
}

void PercentageController::enable()
{
    m_isEnabled = true;
}

void PercentageController::disable()
{
    m_isEnabled = false;
}

void PercentageController::clearHistory()
{
    m_usedWidgetsHistory.clear();
}

void PercentageController::modifyWidgetsValues()
{
    m_inProcess = true;

    double sum = 0.0;
    for (auto it=m_widgetsList.begin(); it!=m_widgetsList.end(); ++it)
        sum += (*it)->value();

    double diff = sum - 100.0;
    if (fabs(diff) <= EPSILON)
    {
        m_inProcess = false;
        return;
    }

    QList<InputWidget*> widgetsToModify;
    for (auto it=m_widgetsList.begin(); it!=m_widgetsList.end(); ++it)
        if (!m_usedWidgetsHistory.contains(*it))
            widgetsToModify.append(*it);

    while (!widgetsToModify.isEmpty() && fabs(diff) > EPSILON)
    {
        int k = widgetsToModify.count();
        double part = diff / k;

        for (auto it=widgetsToModify.begin(); it!=widgetsToModify.end(); ++it)
        {
            double val = (*it)->value();
            double valueDif = diff < 0 ? 100 - val : val;

            if (valueDif < fabs(part))
            {
                (*it)->setValue( diff > 0 ? 0.0 : 100.0);
                diff -= valueDif;
                it = widgetsToModify.erase(it);
                --it;
            }
            else
            {
                (*it)->setValue(val - part);
                diff -= part;
            }
        }
    }

    if (fabs(diff) > EPSILON)
    {
        for (auto it=m_usedWidgetsHistory.begin(); it!=m_usedWidgetsHistory.end(); ++it)
        {
            double val = (*it)->value();
            if (diff < 0)
                val = 100 - val;

            if (val < fabs(diff))
            {
                (*it)->setValue( diff > 0 ? 0.0 : 100.0);
                diff -= val;
            }
            else
            {
                (*it)->setValue(val - diff);
                break;
            }
        }
    }
    m_inProcess = false;
}
