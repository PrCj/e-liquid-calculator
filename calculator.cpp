#include "calculator.h"
#include "result.h"
#include "math.h"

Calculator::Calculator(Base &base, Content &content, QObject *parent) :
    QObject(parent),
    m_base(base),
    m_content(content)
{
    connect(&m_base, SIGNAL(nicotineChanged(double)), this, SLOT(resCalc()));
    connect(&m_base, SIGNAL(VGChanged(double)),       this, SLOT(resCalc()));
    connect(&m_base, SIGNAL(PGChanged(double)),       this, SLOT(resCalc()));
    connect(&m_base, SIGNAL(waterChanged(double)),    this, SLOT(resCalc()));

    connect(&m_content, SIGNAL(nicotineChanged(double)), this, SLOT(resCalc()));
    connect(&m_content, SIGNAL(VGChanged(double)),       this, SLOT(resCalc()));
    connect(&m_content, SIGNAL(PGChanged(double)),       this, SLOT(resCalc()));
    connect(&m_content, SIGNAL(waterChanged(double)),    this, SLOT(resCalc()));
    connect(&m_content, SIGNAL(flavorChanged(double)),   this, SLOT(resCalc()));
    connect(&m_content, SIGNAL(volumeChanged(double)),   this, SLOT(resCalc()));
}

void Calculator::resCalc()
{
    m_error = NoError;
    m_result.setFlavorV(0.0);
    m_result.setNicotineV(0.0);
    m_result.setVGV(0.0);
    m_result.setPGV(0.0);
    m_result.setWaterV(0.0);

    if (m_content.volume() == 0.0)
    {
        emit calcFinished();
        return;
    }
    if (!m_base.isProportionValid())
    {
        m_error = BadBaseProportion;
        emit proportionError(BadBaseProportion);
        return;
    }
    if (!m_content.isProportionValid())
    {
        m_error = BadContenctProportion;
        emit proportionError(BadContenctProportion);
        return;
    }
    if (m_base.nicotine() < m_content.nicotine())
    {
        m_error = BadNicotineCount;
        emit proportionError(BadNicotineCount);
        return;
    }

    double flavorV = m_content.volume() * m_content.flavor() / (100.0 + m_content.flavor());
    double restV = m_content.volume() - flavorV;
    double nicotineV = m_base.nicotine() == 0.0 ? 0.0
              : m_content.nicotine() / m_base.nicotine() * restV;
    double VGV = (restV * m_content.VG() - nicotineV * m_base.VG()) / 100.0;
    double PGV = (restV * m_content.PG() - nicotineV * m_base.PG()) / 100.0;
    double waterV = (restV * m_content.water() - nicotineV * m_base.water()) / 100.0;

    if ( (VGV < 0 && fabs(VGV) > EPSILON)
        || (PGV < 0 && fabs(PGV) > EPSILON)
        || (waterV < 0 && fabs(waterV) > EPSILON) )
    {
        m_error = UnknownError;
        emit proportionError(UnknownError);
        return;
    }

    m_result.setFlavorV(flavorV);
    m_result.setNicotineV(nicotineV);
    m_result.setVGV(VGV);
    m_result.setPGV(PGV);
    m_result.setWaterV(waterV);

    normalize();

    emit calcFinished();
}

void Calculator::normalize()
{
    double ingridients[5];
    ingridients[0] = roundTo2Decimals(m_result.flavorV());
    ingridients[1] = roundTo2Decimals(m_result.nicotineV());
    ingridients[2] = roundTo2Decimals(m_result.VGV());
    ingridients[3] = roundTo2Decimals(m_result.PGV());
    ingridients[4] = roundTo2Decimals(m_result.waterV());

    double resVolume = 0;
    for (int i = 0; i < 5; i++)
        resVolume += ingridients[i];

    double volumeDifference = resVolume - m_content.volume();
    if (fabs(volumeDifference) > EPSILON)
    {
        double percentage[5];
        int changedValues[5];
        int numOfChanges = round(fabs(volumeDifference) * 100.0);
        for (int i = 0; i < 5; i++)
        {
            percentage[i] = ingridients[i] != 0.0 ? 0.01 / ingridients[i] : 100.0;
            changedValues[i] = 0;
        }
        for (int i = 0; i < numOfChanges; i++)
        {
            double minChange = (1 + changedValues[0]) * percentage[0];
            int minIndex = 0;
            for (int j = 1; j < 5; j++)
            {
                if ((1 + changedValues[j]) * percentage[j] < minChange)
                {
                    minChange = (1 + changedValues[j]) * percentage[j];
                    minIndex = j;
                }
            }

            ++changedValues[minIndex];
        }
        for (int i = 0; i < 5; i++)
        {
            if (volumeDifference > 0)
                ingridients[i] -= (double)changedValues[i] / 100.0;
            else
                ingridients[i] += (double)changedValues[i] / 100.0;
        }
    }

    m_result.setFlavorV(ingridients[0]);
    m_result.setNicotineV(ingridients[1]);
    m_result.setVGV(ingridients[2]);
    m_result.setPGV(ingridients[3]);
    m_result.setWaterV(ingridients[4]);

}

double Calculator::roundTo2Decimals(double value)
{
    return round(value * 100.0) / 100.0;
}

Calculator::Error Calculator::error() const
{
    return m_error;
}

const Result &Calculator::result() const
{
    return m_result;
}




