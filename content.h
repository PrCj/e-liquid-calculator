#ifndef CONTENT_H
#define CONTENT_H

#include "base.h"

class Content : public Base
{
    Q_OBJECT

public:
    explicit Content(QObject *parent = 0);

    double volume() const;
    double flavor() const;

protected:
    double m_volume;
    double m_flavor;

public slots:
    void setVolume(double volume);
    void setFlavor(double flavor);

signals:
    void volumeChanged(double value);
    void flavorChanged(double value);

};

#endif // CONTENT_H
