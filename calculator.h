#ifndef CALCULATOR_H
#define CALCULATOR_H

#include <QObject>
#include "content.h"
#include "result.h"

class Calculator : public QObject
{
    Q_OBJECT
public:
    explicit Calculator(Base &base, Content &content, QObject *parent = 0);

    enum Error {NoError, BadBaseProportion, BadContenctProportion, BadNicotineCount, UnknownError};

    const Result &result() const;

    Error error() const;

signals:
    void proportionError(const Error error);
    void calcFinished();

private slots:
    void resCalc();

private:
    Base &m_base;
    Content &m_content;
    Result m_result;

private:
    void normalize();
    double roundTo2Decimals(double value);
    Error m_error;
};

#endif // CALCULATOR_H
