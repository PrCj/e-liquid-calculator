#include "inputwidget.h"
#include "ui_inputwidget.h"

#include <math.h>

InputWidget::InputWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InputWidget)
{
    ui->setupUi(this);
    connect(ui->dsbValue, SIGNAL(valueChanged(double)), this, SIGNAL(valueChanged(double)));
}

InputWidget::~InputWidget()
{
    delete ui;
}

QString InputWidget::title() const
{
    return ui->lbTitle->text();
}

void InputWidget::setTitle(const QString &title)
{
    ui->lbTitle->setText(title);
}

double InputWidget::value() const
{
    return ui->dsbValue->value();
}

void InputWidget::setValue(double value)
{
    ui->dsbValue->setValue(value);
}

void InputWidget::setSuffix(QString suffix)
{
    ui->dsbValue->setSuffix(suffix);
}

void InputWidget::setMaximum(double max)
{
    ui->dsbValue->setMaximum(max);
    ui->sliderValue->setMaximum(max);
}

void InputWidget::turnOnHighlight()
{
    QFont font;
    font.setBold(true);
    ui->lbTitle->setFont(font);
}

void InputWidget::turnOffHighlight()
{
    QFont font;
    font.setBold(false);
    ui->lbTitle->setFont(font);
}

void InputWidget::on_sliderValue_valueChanged(int value)
{
    if (value == 0.0)
    {
        ui->dsbValue->setValue(0.0);
        return;
    }
    double param = ui->dsbValue->value();
    double fractpart, intpart;
    fractpart = modf(param , &intpart);
    ui->dsbValue->setValue(value + fractpart);
}

void InputWidget::on_dsbValue_valueChanged(double arg1)
{
    ui->sliderValue->setValue(arg1);
}
