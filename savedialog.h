#ifndef SAVEDIALOG_H
#define SAVEDIALOG_H

#include <QDialog>

#include "settings.h"

namespace Ui {
class SaveDialog;
}

class SaveDialog : public QDialog
{
    Q_OBJECT

public:
    explicit SaveDialog(Settings &settings, QWidget *parent = 0);
    ~SaveDialog();

    void setDescription(const QString &value);
    QString name() const;

private slots:
    void on_pbSave_clicked();

private:
    Ui::SaveDialog *ui;
    Settings &m_settings;
    QString m_name;
};

#endif // SAVEDIALOG_H
