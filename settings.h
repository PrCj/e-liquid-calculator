#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>
#include "content.h"

class Settings : public QObject
{
    Q_OBJECT
public:
    explicit Settings(Base &base, Content &content, QObject *parent = 0);

    void saveLast(const QString &description="");
    void loadLast();
    void load(const QString &name);
    void save(const QString &name, const QString &description="");
    void remove(const QString &name);
    bool contains(const QString &name);

    QStringList getSavings() const;
    QString getDescription(const QString name) const;
    QString getDescription() const;
    QStringList reservedWords() const;

    QString lastUsedName() const;
    void clearLastUsedName();

private:
    Base &m_base;
    Content &m_content;
    QString m_lastUsedName;
};

#endif // SETTINGS_H
