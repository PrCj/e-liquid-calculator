#ifndef INPUTWIDGET_H
#define INPUTWIDGET_H

#include <QWidget>

namespace Ui {
class InputWidget;
}

class InputWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InputWidget(QWidget *parent = 0);
    ~InputWidget();

    QString title() const;
    double value() const;

private:
    Ui::InputWidget *ui;

public slots:
    void setValue(double value);
    void setTitle(const QString &title);
    void setSuffix(QString suffix);
    void setMaximum(double max);
    void turnOnHighlight();
    void turnOffHighlight();

signals:
    void valueChanged(double);
private slots:
    void on_sliderValue_valueChanged(int value);
    void on_dsbValue_valueChanged(double arg1);
};

#endif // INPUTWIDGET_H
