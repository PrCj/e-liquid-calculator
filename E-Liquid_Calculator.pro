#-------------------------------------------------
#
# Project created by QtCreator 2016-05-24T01:29:32
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = E-Liquid_Calculator
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp \
    liquid.cpp \
    base.cpp \
    content.cpp \
    calculator.cpp \
    result.cpp \
    inputwidget.cpp \
    settings.cpp \
    outputwidget.cpp \
    percentagecontroller.cpp \
    savedialog.cpp \
    loaddialog.cpp

HEADERS  += mainwindow.h \
    liquid.h \
    base.h \
    content.h \
    calculator.h \
    result.h \
    inputwidget.h \
    settings.h \
    outputwidget.h \
    percentagecontroller.h \
    savedialog.h \
    loaddialog.h

FORMS    += mainwindow.ui \
    inputwidget.ui \
    outputwidget.ui \
    savedialog.ui \
    loaddialog.ui
