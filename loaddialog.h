#ifndef LOADDIALOG_H
#define LOADDIALOG_H

#include <QDialog>

#include "settings.h"

namespace Ui {
class LoadDialog;
}

class QListWidgetItem;

class LoadDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoadDialog(Settings &settings, QWidget *parent = 0);
    ~LoadDialog();

    QString name() const;

private slots:
    void initList();
    void on_pbLoad_clicked();

    void on_listWidget_itemSelectionChanged();

    void on_pbDelete_clicked();

private:
    Ui::LoadDialog *ui;
    Settings &m_settings;
    QString m_name;
};

#endif // LOADDIALOG_H
