#include "savedialog.h"
#include "ui_savedialog.h"

#include <QMessageBox>

SaveDialog::SaveDialog(Settings &settings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SaveDialog),
    m_settings(settings)
{
    ui->setupUi(this);

    ui->leName->setText( m_settings.lastUsedName());
    ui->leName->selectAll();
}

SaveDialog::~SaveDialog()
{
    delete ui;
}

void SaveDialog::setDescription(const QString &value)
{
    ui->pteDescription->setPlainText(value);
}

void SaveDialog::on_pbSave_clicked()
{
    QString name = ui->leName->text();
    if (name.isEmpty())
    {
        QMessageBox::information(this, tr("Name is empty"), tr("Name can't be empty!"));
        return;
    }

    if (m_settings.contains(name))
        if ( QMessageBox::question(this, tr("Duplicate names"), tr("Do you want replace '%1'?").arg(name), QMessageBox::Yes, QMessageBox::No) != QMessageBox::Yes)
            return;

    if (m_settings.reservedWords().contains(name))
    {
        QMessageBox::information(this, tr("Registered word"), tr("You can't use '%1' as name").arg(name));
        return;
    }

    m_name = name;
    m_settings.save(name, ui->pteDescription->toPlainText());

    accept();
}

QString SaveDialog::name() const
{
    return m_name;
}
