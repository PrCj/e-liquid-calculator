#ifndef RESULT_H
#define RESULT_H

#include <QObject>

class Result : public QObject
{
    Q_OBJECT

public:
    explicit Result(QObject *parent = 0);

    double flavorV() const;
    double nicotineV() const;
    double VGV() const;
    double PGV() const;
    double waterV() const;

signals:

public slots:
    void setFlavorV(double flavorV);
    void setNicotineV(double nicotineV);
    void setVGV(double VGV);
    void setPGV(double PGV);
    void setWaterV(double waterV);

private:
    double m_flavorV;
    double m_nicotineV;
    double m_VGV;
    double m_PGV;
    double m_waterV;
};

#endif // RESULT_H
