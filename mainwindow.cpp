#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QCloseEvent>
#include "settings.h"
#include "savedialog.h"
#include "loaddialog.h"

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow),
    m_base(parent),
    m_content(parent),
    m_pcBase(parent),
    m_pcContent(parent),
    m_calculator(m_base, m_content, parent),
    m_settings(m_base, m_content, parent)
{
    ui->setupUi(this);
    prepareUI();
    retranslateUI();
    setConnections();

    m_settings.loadLast();
    ui->pteDescription->setPlainText( m_settings.getDescription());

    m_pcBase.enable();
    m_pcContent.enable();
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::prepareUI()
{
    ui->iwdNicotine->setSuffix(tr(" mg/ml"));
    ui->iwdVG->setSuffix(tr("%"));
    ui->iwdPG->setSuffix(tr("%"));
    ui->iwdWater->setSuffix(tr("%"));
    ui->iwdContVolume->setSuffix(tr(" ml"));
    ui->iwdContNicotine->setSuffix(tr(" mg/ml"));
    ui->iwdContVG->setSuffix(tr("%"));
    ui->iwdContPG->setSuffix(tr("%"));
    ui->iwdContWater->setSuffix(tr("%"));
    ui->iwdContFlavor->setSuffix(tr("%"));

    ui->owdFlavor->setSuffix(tr(" ml"));
    ui->owdNicotine->setSuffix(tr(" ml"));
    ui->owdPG->setSuffix(tr(" ml"));
    ui->owdVG->setSuffix(tr(" ml"));
    ui->owdWater->setSuffix(tr(" ml"));

    ui->iwdContVolume->setMaximum(500.0);
    ui->iwdContFlavor->setMaximum(20.0);
}

void MainWindow::retranslateUI()
{
    ui->iwdNicotine->setTitle(tr("Nicotine"));
    ui->iwdVG->setTitle(tr("VG"));
    ui->iwdPG->setTitle(tr("PG"));
    ui->iwdWater->setTitle(tr("Water"));
    ui->iwdContVolume->setTitle(tr("Volume"));
    ui->iwdContNicotine->setTitle(tr("Nicotine"));
    ui->iwdContVG->setTitle(tr("VG"));
    ui->iwdContPG->setTitle(tr("PG"));
    ui->iwdContWater->setTitle(tr("Water"));
    ui->iwdContFlavor->setTitle(tr("Flavor"));

    ui->owdFlavor->setTitle(tr("Flavor"));
    ui->owdNicotine->setTitle(tr("Nicotine"));
    ui->owdPG->setTitle(tr("PG"));
    ui->owdVG->setTitle(tr("VG"));
    ui->owdWater->setTitle(tr("Water"));
}

void MainWindow::setConnections()
{
    m_pcBase.addWidget(ui->iwdVG);
    m_pcBase.addWidget(ui->iwdPG);
    m_pcBase.addWidget(ui->iwdWater);

    m_pcContent.addWidget(ui->iwdContVG);
    m_pcContent.addWidget(ui->iwdContPG);
    m_pcContent.addWidget(ui->iwdContWater);

    connect(ui->iwdNicotine,  SIGNAL(valueChanged(double)), &m_base, SLOT(setNicotine(double)));
    connect(ui->iwdVG,        SIGNAL(valueChanged(double)), &m_base, SLOT(setVG(double)));
    connect(ui->iwdPG,        SIGNAL(valueChanged(double)), &m_base, SLOT(setPG(double)));
    connect(ui->iwdWater,     SIGNAL(valueChanged(double)), &m_base, SLOT(setWater(double)));

    connect(ui->iwdContVolume,    SIGNAL(valueChanged(double)), &m_content, SLOT(setVolume(double)));
    connect(ui->iwdContNicotine,  SIGNAL(valueChanged(double)), &m_content, SLOT(setNicotine(double)));
    connect(ui->iwdContVG,        SIGNAL(valueChanged(double)), &m_content, SLOT(setVG(double)));
    connect(ui->iwdContPG,        SIGNAL(valueChanged(double)), &m_content, SLOT(setPG(double)));
    connect(ui->iwdContWater,     SIGNAL(valueChanged(double)), &m_content, SLOT(setWater(double)));
    connect(ui->iwdContFlavor,    SIGNAL(valueChanged(double)), &m_content, SLOT(setFlavor(double)));

    connect(&m_calculator, SIGNAL(proportionError(Error)), this, SLOT(showErrorMessage()));
    connect(&m_calculator, SIGNAL(calcFinished()), this, SLOT(printResult()));

    connect(&m_base, SIGNAL(nicotineChanged(double)),   ui->iwdNicotine, SLOT(setValue(double)) );
    connect(&m_base, SIGNAL(PGChanged(double)),         ui->iwdPG,       SLOT(setValue(double)) );
    connect(&m_base, SIGNAL(VGChanged(double)),         ui->iwdVG,       SLOT(setValue(double)) );
    connect(&m_base, SIGNAL(waterChanged(double)),      ui->iwdWater,    SLOT(setValue(double)) );

    connect(&m_content, SIGNAL(nicotineChanged(double)),    ui->iwdContNicotine, SLOT(setValue(double)) );
    connect(&m_content, SIGNAL(PGChanged(double)),          ui->iwdContPG,       SLOT(setValue(double)) );
    connect(&m_content, SIGNAL(VGChanged(double)),          ui->iwdContVG,       SLOT(setValue(double)) );
    connect(&m_content, SIGNAL(waterChanged(double)),       ui->iwdContWater,    SLOT(setValue(double)) );
    connect(&m_content, SIGNAL(volumeChanged(double)),      ui->iwdContVolume,   SLOT(setValue(double)) );
    connect(&m_content, SIGNAL(flavorChanged(double)),      ui->iwdContFlavor,   SLOT(setValue(double)) );

    connect(ui->pbLoad,  SIGNAL(clicked(bool)), this, SLOT(load()) );
    connect(ui->pbSave,  SIGNAL(clicked(bool)), this, SLOT(save()) );
    connect(ui->pbClear, SIGNAL(clicked(bool)), this, SLOT(clear()) );
}

void MainWindow::loadDefaultValues()
{
    m_base.setNicotine( 100.0 );
    m_base.setPG( 100.0 );
    m_base.setVG( 0.0 );
    m_base.setWater( 0.0 );

    m_content.setNicotine( 6.0 );
    m_content.setPG( 45.0 );
    m_content.setVG( 45.0 );
    m_content.setWater( 10.0 );
    m_content.setVolume( 10.0 );
    m_content.setFlavor( 5.0 );
}

void MainWindow::showErrorMessage()
{
    switch (m_calculator.error()) {
    case Calculator::BadBaseProportion:
        ui->lbError->setText(tr("An error has occurred. "
                                "Result can't be calculated because you have wrong base proportion"));
        break;
    case Calculator::BadContenctProportion:
        ui->lbError->setText(tr("An error has occurred. "
                                "Result can't be calculated because you have wrong content proportion"));
        break;
    case Calculator::BadNicotineCount:
        ui->lbError->setText(tr("An error has occurred. "
                                "Result can't be calculated because you cann't have this count of nicotine"));
        break;
    case Calculator::UnknownError:
        ui->lbError->setText(tr("An error has occurred. "
                                "Result can't be calculated."));
        break;
    case Calculator::NoError:
    default:
        break;
    }

    ui->owdNicotine->setValue("-");
    ui->owdVG->setValue("-");
    ui->owdPG->setValue("-");
    ui->owdWater->setValue("-");
    ui->owdFlavor->setValue("-");
}

void MainWindow::printResult()
{
    ui->lbError->clear();
    const Result &result = m_calculator.result();
    ui->owdNicotine->setValue(QString::number(result.nicotineV()));
    ui->owdVG->setValue(QString::number(result.VGV()));
    ui->owdPG->setValue(QString::number(result.PGV()));
    ui->owdWater->setValue(QString::number(result.waterV()));
    ui->owdFlavor->setValue(QString::number(result.flavorV()));
}

void MainWindow::save()
{
    SaveDialog dlg(m_settings, this);
    dlg.setDescription(ui->pteDescription->toPlainText());
    dlg.exec();
}

void MainWindow::load()
{
    LoadDialog dlg(m_settings, this);

    m_pcBase.disable();
    m_pcContent.disable();

    if (dlg.exec() == QDialog::Accepted)
    {
        m_pcBase.clearHistory();
        m_pcContent.clearHistory();

        ui->pteDescription->setPlainText(m_settings.getDescription(m_settings.lastUsedName()));
    }

    m_pcBase.enable();
    m_pcContent.enable();
}

void MainWindow::clear()
{
    m_pcBase.disable();
    m_pcContent.disable();

    m_pcBase.clearHistory();
    m_pcContent.clearHistory();

    loadDefaultValues();

    m_settings.clearLastUsedName();
    ui->pteDescription->clear();

    m_pcBase.enable();
    m_pcContent.enable();
}

void MainWindow::closeEvent(QCloseEvent *event)
{
    m_settings.saveLast(ui->pteDescription->toPlainText());
    event->accept();
}

