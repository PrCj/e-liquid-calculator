#include "loaddialog.h"
#include "ui_loaddialog.h"

LoadDialog::LoadDialog(Settings &settings, QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoadDialog),
    m_settings(settings),
    m_name("")
{
    ui->setupUi(this);
    initList();
}

LoadDialog::~LoadDialog()
{
    delete ui;
}

void LoadDialog::initList()
{
    ui->listWidget->clear();
    ui->listWidget->addItems( m_settings.getSavings() );

    ui->pteDescription->clear();
    ui->pbLoad->setEnabled(false);
    ui->pbDelete->setEnabled(false);
}

void LoadDialog::on_pbLoad_clicked()
{
    m_name = ui->listWidget->selectedItems().first()->text();
    m_settings.load(m_name);

    accept();
}

QString LoadDialog::name() const
{
    return m_name;
}

void LoadDialog::on_listWidget_itemSelectionChanged()
{
    if (ui->listWidget->selectedItems().count() < 1)
        return;

    QListWidgetItem *item = ui->listWidget->selectedItems().first();

    ui->pbLoad->setEnabled(true);
    ui->pbDelete->setEnabled(true);
    QString name = item->text();
    ui->pteDescription->setPlainText( m_settings.getDescription(name) );
}

void LoadDialog::on_pbDelete_clicked()
{
    m_name = ui->listWidget->selectedItems().first()->text();
    m_settings.remove(m_name);
    initList();
}
