#include "base.h"

Base::Base(QObject *parent) : Liquid(parent)
{
    m_nicotine = 0.0;
}

double Base::nicotine() const
{
    return m_nicotine;
}

void Base::setNicotine(double nicotine)
{
    nicotine = volumeValidation(nicotine);
    if (nicotine != m_nicotine)
    {
        m_nicotine = nicotine;
        emit nicotineChanged(m_nicotine);
    }
}

double Base::volumeValidation(double value)
{
    if (value < 0.0)
        return 0.0;
    return value;
}

