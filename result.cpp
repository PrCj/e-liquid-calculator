#include "result.h"

Result::Result(QObject *parent) : QObject(parent)
{
    m_flavorV = 0.0;
    m_nicotineV = 0.0;
    m_VGV = 0.0;
    m_PGV = 0.0;
    m_waterV = 0.0;
}

double Result::flavorV() const
{
    return m_flavorV;
}

void Result::setFlavorV(double flavorV)
{
    m_flavorV = flavorV;
}

double Result::nicotineV() const
{
    return m_nicotineV;
}

void Result::setNicotineV(double nicotineV)
{
    m_nicotineV = nicotineV;
}

double Result::VGV() const
{
    return m_VGV;
}

void Result::setVGV(double VG)
{
    m_VGV = VG;
}

double Result::PGV() const
{
    return m_PGV;
}

void Result::setPGV(double PG)
{
    m_PGV = PG;
}

double Result::waterV() const
{
    return m_waterV;
}

void Result::setWaterV(double water)
{
    m_waterV = water;
}
