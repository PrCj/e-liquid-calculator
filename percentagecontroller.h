#ifndef PERCENTAGECONTROLLER_H
#define PERCENTAGECONTROLLER_H

#include <QObject>
#include <QList>
#include <QQueue>

class InputWidget;

class PercentageController : public QObject
{
    Q_OBJECT
public:
    explicit PercentageController(QObject *parent = 0);

    void addWidget(InputWidget *widget);
    void removeWidget(InputWidget *widget);
    void enable();
    void disable();
    void clearHistory();

private slots:
    void widgetValueChanged(double value);

private:
    void modifyWidgetsValues();

private:
    bool m_isEnabled;
    bool m_inProcess;
    QList<InputWidget *> m_widgetsList;
    QQueue<InputWidget *> m_usedWidgetsHistory;
};

#endif // PERCENTAGECONTROLLER_H
