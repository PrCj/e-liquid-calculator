#ifndef OUTPUTWIDGET_H
#define OUTPUTWIDGET_H

#include <QWidget>

namespace Ui {
class OutputWidget;
}

class OutputWidget : public QWidget
{
    Q_OBJECT

public:
    explicit OutputWidget(QWidget *parent = 0);
    ~OutputWidget();

    QString title() const;
    QString value() const;
    QString suffix() const;

public slots:
    void setTitle(const QString &title);
    void setValue(const QString &value);
    void setSuffix(const QString &suffix);

private:
    Ui::OutputWidget *ui;
};

#endif // OUTPUTWIDGET_H
