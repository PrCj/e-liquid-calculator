#include "content.h"

Content::Content(QObject *parent) : Base(parent)
{
    m_volume = 0.0;
    m_flavor = 0.0;
}

double Content::volume() const
{
    return m_volume;
}

void Content::setVolume(double volume)
{
    volume = volumeValidation(volume);
    if (volume != m_volume)
    {
        m_volume = volume;
        emit volumeChanged(m_volume);
    }
}

double Content::flavor() const
{
    return m_flavor;
}

void Content::setFlavor(double flavor)
{
    flavor = percentageValidation(flavor);
    if (flavor != m_flavor)
    {
        m_flavor = flavor;
        emit flavorChanged(m_flavor);
    }
}




