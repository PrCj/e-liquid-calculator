#include "liquid.h"
#include <cmath>

double Liquid::VG() const
{
    return m_VG;
}

void Liquid::setVG(double VG)
{
    VG = percentageValidation(VG);

    if (VG == m_VG)
        return;

    m_VG = VG;
    emit VGChanged(m_VG);

    if (isProportionValid())
        emit validProportion();
    else
        emit notValidProportion();
}

double Liquid::PG() const
{
    return m_PG;
}

void Liquid::setPG(double PG)
{
    PG = percentageValidation(PG);

    if (PG == m_PG)
        return;

    m_PG = PG;
    emit PGChanged(m_PG);

    if (isProportionValid())
        emit validProportion();
    else
        emit notValidProportion();
}

double Liquid::water() const
{
    return m_water;
}

bool Liquid::isProportionValid()
{
    return fabs(m_VG + m_PG + m_water - 100.0) <= EPSILON;
}

void Liquid::setWater(double water)
{
    water = percentageValidation(water);

    if (water == m_water)
        return;

    m_water = water;
    emit waterChanged(m_water);

    if (isProportionValid())
        emit validProportion();
    else
        emit notValidProportion();
}

double Liquid::percentageValidation(double value)
{
    if (value > 100.0)
        return 100.0;
    if (value < 0.0)
        return 0.0;
    return value;
}

Liquid::Liquid(QObject *parent) : QObject(parent)
{
    m_VG = 0.0;
    m_PG = 0.0;
    m_water = 0.0;
}

