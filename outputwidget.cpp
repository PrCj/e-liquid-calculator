#include "outputwidget.h"
#include "ui_outputwidget.h"

OutputWidget::OutputWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::OutputWidget)
{
    ui->setupUi(this);
}

OutputWidget::~OutputWidget()
{
    delete ui;
}

QString OutputWidget::title() const
{
    return ui->lbTitle->text();
}

void OutputWidget::setTitle(const QString &title)
{
    ui->lbTitle->setText(title);
}

QString OutputWidget::value() const
{
    return ui->lbValue->text();
}

void OutputWidget::setValue(const QString &value)
{
    ui->lbValue->setText(value);
}

QString OutputWidget::suffix() const
{
    return ui->lbSuffix->text();
}

void OutputWidget::setSuffix(const QString &suffix)
{
    ui->lbSuffix->setText(suffix);
}
