#ifndef LIQUID_H
#define LIQUID_H
#define EPSILON 0.000001

#include <QObject>

class Liquid : public QObject
{
    Q_OBJECT
protected:
    double m_VG;
    double m_PG;
    double m_water;

public:
    explicit Liquid(QObject *parent = 0);

    double VG() const;
    double PG() const;
    double water() const;
    bool isProportionValid();

signals:
    void VGChanged(double value);
    void PGChanged(double value);
    void waterChanged(double value);
    void validProportion();
    void notValidProportion();

public slots:
    void setVG(double VG);
    void setPG(double PG);
    void setWater(double water);

protected:
    double percentageValidation(double value);
};



#endif // LIQUID_H
