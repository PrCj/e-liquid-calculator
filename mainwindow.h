#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include "base.h"
#include "content.h"
#include "calculator.h"
#include "settings.h"
#include "percentagecontroller.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private:
    Ui::MainWindow *ui;
    Base m_base;
    Content m_content;
    PercentageController m_pcBase;
    PercentageController m_pcContent;
    Calculator m_calculator;
    Settings m_settings;

private slots:
    void closeEvent(QCloseEvent *event);
    void prepareUI();
    void retranslateUI();
    void setConnections();
    void loadDefaultValues();
    void showErrorMessage();
    void printResult();
    void save();
    void load();
    void clear();
};


#endif // MAINWINDOW_H
